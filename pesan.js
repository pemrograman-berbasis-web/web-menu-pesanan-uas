document.getElementById('tekan').addEventListener("click", submit)

row = null

function submit(){
    var dataMasuk = ambilData()
    var bacaData = bacaDataLS(dataMasuk)
    if(dataMasuk == false){
        msg.innerHTML = `<h4 class="text-danger mt-5">Please Insert The Data!</h4>`
    } else{
        if(row == null){
            insert(bacaData)
            msg.innerHTML = `<h4 class="text-success mt-5">Data Inserted</h4>`
        }else{
            update()
            msg.innerHTML = `<h4 class="text-info mt-5">Data Updated</h4>`
        }
    }
    document.getElementById('form').reset()
}

function ambilData(){
    var no = document.getElementById('no1').value
    var menu = document.getElementById('menu1').value
    var jml = document.getElementById('jml1').value

    var arr=[no,menu,jml]
    if(arr.includes('')){
        return false
    } else{
        return arr
    }
}

function bacaDataLS(dataMasuk){
    //nyimpen data ke localStorage
    var n = localStorage.setItem('No Meja', dataMasuk[0])
    var m = localStorage.setItem('Menu Pesanan', dataMasuk[1])
    var j = localStorage.setItem('Jumlah Pesanan', dataMasuk[2])

    //nyimpen data dari local ke table
    var n1 = localStorage.getItem('No Meja',n)
    var m1 = localStorage.getItem('Menu Pesanan',m)
    var j1 = localStorage.getItem('Jumlah Pesanan',j)

    var arr = [n1,m1,j1]
    return arr
}

//masukkan
function insert(bacaData){
    var row = table.insertRow()
    row.insertCell(0).innerHTML = bacaData[0]
    row.insertCell(1).innerHTML = bacaData[1]
    row.insertCell(2).innerHTML = bacaData[2]
    row.insertCell(3).innerHTML = `<button onclick = edit(this) >Edit</button>
                                <button onclick = remove(this)>Delete</button>`
}

//edit
function edit(td){
    row = td.parentElement.parentElement
    document.getElementById('no1').value = row.cells[0].innerHTML
    document.getElementById('menu1').value = row.cells[1].innerHTML
    document.getElementById('jml1').value = row.cells[2].innerHTML
}

//update data
function update(){
    row.cells[0].innerHTML = document.getElementById('no1').value
    row.cells[1].innerHTML = document.getElementById('menu1').value
    row.cells[2].innerHTML = document.getElementById('jml1').value
    row = null
}

//delete
function remove(td){
    var jwb = confirm('Are you sure want to delete this row?')
    if(jwb == true){
        row = td.parentElement.parentElement
        document.getElementById('table').deleteRow(row.rowIndex)
    }
}